package com.twuc.webApp.web;

import com.twuc.webApp.contracts.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentConversionNotSupportedException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.time.LocalDateTime;

@ControllerAdvice
public class GlobalErrorHandler {
    @ExceptionHandler({
        MethodArgumentNotValidException.class,
        MethodArgumentTypeMismatchException.class,
        MethodArgumentConversionNotSupportedException.class
    })
    public ResponseEntity handleValidationException(Exception error) {
        return ResponseEntity.badRequest().body(
            new ErrorResponse(
                LocalDateTime.now().toString(),
                error.getMessage(),
                error.getClass().getTypeName()));
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity handleException(Exception error) {
        ResponseStatus annotation = error.getClass().getAnnotation(ResponseStatus.class);
        HttpStatus status = annotation == null ? HttpStatus.INTERNAL_SERVER_ERROR : annotation.code();
        return ResponseEntity.status(status).body(
            new ErrorResponse(
                LocalDateTime.now().toString(),
                error.getMessage(),
                error.getClass().getTypeName()));
    }
}
